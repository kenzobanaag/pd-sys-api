package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;
import org.springframework.data.jpa.domain.Specification;

public interface AppointmentSpecification {
    Specification<Appointment> appointmentQuery(AppointmentMeta metaModel);
}
