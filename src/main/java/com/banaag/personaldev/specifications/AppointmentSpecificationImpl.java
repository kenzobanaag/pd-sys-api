package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class AppointmentSpecificationImpl implements AppointmentSpecification
{
    @Override
    public Specification<Appointment> appointmentQuery(AppointmentMeta metaModel) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.get("user").get("userId"), metaModel.getUserId()));

            if(metaModel.getDate().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("date"), metaModel.getDate().get()));

            if(metaModel.getFromDate().isPresent() && metaModel.getToDate().isPresent()
                    && metaModel.getFromDate().get().compareTo(metaModel.getToDate().get()) <= 0) {
                predicates.add(criteriaBuilder.between(root.get("date"),
                        metaModel.getFromDate().get(), metaModel.getToDate().get()));
            }

            if(metaModel.getDescKeywords().isPresent())
                predicates.add(criteriaBuilder.like(
                        root.get("description"), "%"+metaModel.getDescKeywords().get()+"%"));


            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
