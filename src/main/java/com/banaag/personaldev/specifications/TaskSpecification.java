package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.metamodels.TaskMeta;
import org.springframework.data.jpa.domain.Specification;

public interface TaskSpecification {
    Specification<Task> taskQuery(TaskMeta taskMeta);
}
