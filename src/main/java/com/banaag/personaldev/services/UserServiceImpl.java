package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Settings;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepo;

    @Override
    public Iterable<User> getUsers() {
        return userRepo.findAll();
    }

    @Override
    public Optional<User> getUserById(long id) {
        return userRepo.findById(id);
    }

    @Override
    public long addUser(User user) {
        Settings defaultSettings = new Settings();
        defaultSettings.setUser(user);
        user.setSettings(defaultSettings);
        user.setAppointments(new ArrayList<>());
        user.setTasks(new ArrayList<>());
        if(!user.isComplete())
            throw new BadRequestException("Missing required fields");
        // user id will always be zero because it hasn't been created, make sure no user has the same fields
        validateNoDuplicateUser(user.getUsername(), user.getEmail(), user.getUserId());

        return userRepo.save(user).getUserId();
    }

    @Override
    public long updateUser(long id, User user) {
        User tempUser = getUserById(id).isPresent() ? getUserById(id).get() : null;
        if(tempUser == null)
            throw new ResourceNotFoundException("User doesn't exist");

        if(user.getSettings() != null)
            tempUser.getSettings().patch(user.getSettings());
        tempUser.patch(user);

        // validating after patching makes it convenient since we don't have to check for nulls if
        // email|username is not included in patch
        validateNoDuplicateUser(tempUser.getUsername(), tempUser.getEmail(), tempUser.getUserId());

        return userRepo.save(tempUser).getUserId();
    }

    @Override
    public void deleteUserById(long id) {
        validateUserExists(id);
        userRepo.deleteById(id);
    }

    private void validateNoDuplicateUser(String username, String email, long id) {
        if(userRepo.countByUsernameAndUserIdNot(username, id) > 0)
            throw new BadRequestException("Username already taken");
        if(userRepo.countByEmailAndUserIdNot(email, id) > 0)
            throw new BadRequestException("Email already taken");
    }

    private void validateUserExists(long id) {
        if(!userRepo.existsById(id))
            throw new ResourceNotFoundException("User doesn't exist");
    }
}