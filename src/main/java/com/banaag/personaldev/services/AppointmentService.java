package com.banaag.personaldev.services;

import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;

import java.util.Optional;

public interface AppointmentService {
    Iterable<Appointment> getAppointments(AppointmentMeta appointmentMeta);
    Optional<Appointment> getAppointmentById(long userId, long id);
    long addAppointment(long userId, Appointment appointment);
    long updateAppointment(long userId, long id, Appointment appointment);
    void deleteAppointmentById(long userId, long id);
}
