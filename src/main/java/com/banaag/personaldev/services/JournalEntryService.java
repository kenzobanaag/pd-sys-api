package com.banaag.personaldev.services;

import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;

import java.util.Optional;

public interface JournalEntryService {
    Iterable<JournalEntry> getJournalEntries(JournalEntryMeta journalEntryMeta);
    Optional<JournalEntry> getJournalEntryById(long userId, long entryId);
    long addJournalEntry(long userId, JournalEntry entry);
    long updateJournalEntry(long userId, long entryId, JournalEntry entry);
    void deleteJournalEntryById(long userId, long entryId);
}
