package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;
import com.banaag.personaldev.repository.AppointmentRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.AppointmentSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AppointmentServiceImpl implements AppointmentService{

    @Autowired
    AppointmentRepository appointmentRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    AppointmentSpecification appointmentSpec;

    @Override
    public Iterable<Appointment> getAppointments(AppointmentMeta meta) {
        if(!userRepo.existsById(meta.getUserId()))
            throw new ResourceNotFoundException("User doesn't exist");
        return appointmentRepo.findAll(appointmentSpec.appointmentQuery(meta));
    }

    @Override
    public Optional<Appointment> getAppointmentById(long userId, long id) {
        return appointmentRepo.findByUserUserIdAndAppointmentId(userId, id);
    }

    @Override
    public long addAppointment(long userId, Appointment appointment) {
        if(userRepo.findById(userId).isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        if(!appointment.isComplete())
            throw new BadRequestException("Missing required fields");
        User user = userRepo.findById(userId).get();
        appointment.setUser(user);
        user.getAppointments().add(appointment);
        userRepo.save(user);
        return user.getAppointments().size() > 0 ?
                user.getAppointments().get(user.getAppointments().size() - 1).getAppointmentId() : -1;
    }

    @Override
    public long updateAppointment(long userId, long id, Appointment appointment) {
        Optional<User> tempUser = userRepo.findById(userId);
        if(tempUser.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        Optional<Appointment> tempApt = appointmentRepo.findById(id);
        if(tempApt.isEmpty())
            throw new ResourceNotFoundException("Appointment doesn't exist");
        Appointment apt = tempApt.get();
        User user = tempUser.get();
        int index = user.getAppointments().indexOf(apt);
        apt.patch(appointment);
        user.getAppointments().set(index, apt);
        userRepo.save(user);
        return id;
    }

    @Override
    public void deleteAppointmentById(long userId, long id) {
        Optional<User> tempUser = userRepo.findById(userId);
        if(tempUser.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        Optional<Appointment> tempApt = appointmentRepo.findById(id);
        if(tempApt.isEmpty())
            throw new ResourceNotFoundException("Appointment doesn't exist");

        tempUser.get().getAppointments().remove(tempApt.get());
        appointmentRepo.deleteById(id);
    }
}
