package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;
import com.banaag.personaldev.repository.JournalEntryRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.JournalEntrySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JournalEntryServiceImpl implements JournalEntryService {

    @Autowired
    JournalEntryRepository entryRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    JournalEntrySpecification jEntrySpec;

    @Override
    public Iterable<JournalEntry> getJournalEntries(JournalEntryMeta metaModel) {
        if(!userRepo.existsById(metaModel.getUserId()))
            throw new ResourceNotFoundException("User doesn't exist");
        return entryRepo.findAll(jEntrySpec.journalQuery(metaModel));
    }

    @Override
    public Optional<JournalEntry> getJournalEntryById(long userId, long entryId) {
        return entryRepo.findByUserUserIdAndEntryId(userId, entryId);
    }

    @Override
    public long addJournalEntry(long userId, JournalEntry entry) {
        Optional<User> user = userRepo.findById(userId);
        if(user.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        if(!entry.isComplete())
            throw new BadRequestException("Missing required fields");

        entry.setUser(user.get());
        return entryRepo.save(entry).getEntryId();
    }

    @Override
    public long updateJournalEntry(long userId, long entryId, JournalEntry entry) {
        // get user
        Optional<User> user = userRepo.findById(userId);
        if(user.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        Optional<JournalEntry> tempEntry = entryRepo.findById(entryId);
        if(tempEntry.isEmpty())
            throw new ResourceNotFoundException("Entry doesn't exist");

        JournalEntry patchedEntry = tempEntry.get();
        patchedEntry.patch(entry);
        return entryRepo.save(patchedEntry).getEntryId();
    }

    @Override
    public void deleteJournalEntryById(long userId, long entryId) {
        if(!userRepo.existsById(userId))
            throw new ResourceNotFoundException("User doesn't exist");
        if(!entryRepo.existsById(entryId))
            throw new ResourceNotFoundException("Entry doesn't exist");
        entryRepo.deleteById(entryId);
    }
}
