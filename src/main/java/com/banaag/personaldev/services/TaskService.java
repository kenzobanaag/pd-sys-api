package com.banaag.personaldev.services;

import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.metamodels.TaskMeta;

import java.util.Optional;

public interface TaskService {
    Iterable<Task> getTasks(TaskMeta taskMeta);
    Optional<Task> getTaskById(long userId, long taskId);
    long addTask(long userId, Task task);
    long updateTask(long userId, long taskId, Task task);
    void deleteTaskById(long userId, long taskId);
    void deleteAllTasksById(long userId, Optional<Boolean> done);
}