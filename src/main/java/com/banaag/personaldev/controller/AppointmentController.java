package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;
import com.banaag.personaldev.services.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.Optional;

@RestController
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    
    @GetMapping("/users/{userId}/appointments")
    public ResponseEntity<Object> getAppointments(@PathVariable("userId") long userId,
                                          @RequestParam(required = false) Optional<Date> date,
                                          @RequestParam(required = false) Optional<Date> fromDate,
                                          @RequestParam(required = false) Optional<Date> toDate,
                                          @RequestParam(required = false) Optional<String> descKeywords) {
        return ResponseEntity.ok().body(appointmentService.getAppointments(
                new AppointmentMeta(userId, date, fromDate, toDate, descKeywords)));
    }

    @PostMapping("/users/{userId}/appointments")
    public ResponseEntity<Object> addAppointment(@PathVariable("userId") long userId,
                                                 @RequestBody Appointment appointment) {
        long createdAppointmentId = appointmentService.addAppointment(userId, appointment);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.LOCATION,
                    "/users/"+userId+"/appointments/"+createdAppointmentId);
            return ResponseEntity
                    .created(new URI("/users/"+userId+"/appointment"+createdAppointmentId))
                    .headers(header)
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Error in POST /appointments");
        }
    }

    @GetMapping("/users/{userId}/appointments/{appointmentId}")
    public ResponseEntity<Object> getAppointmentById(@PathVariable("userId") long userId, @PathVariable("appointmentId")
                                             long appointmentId) {
        Optional<Appointment> appointment = appointmentService.getAppointmentById(userId, appointmentId);
        if(appointment.isPresent())
            return ResponseEntity.ok().body(appointment);
        else
            return ResponseEntity.notFound().build();
    }

    @PatchMapping("/users/{userId}/appointments/{appointmentId}")
    public ResponseEntity<Object> updateAppointment(@PathVariable("userId") long userId, @PathVariable("appointmentId")
            long appointmentId, @RequestBody Appointment appointment) {
        long tempAppointmentId = appointmentService.updateAppointment(userId, appointmentId, appointment);
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.LOCATION, "/users/"+userId+"/appointments/"+tempAppointmentId);
        return ResponseEntity
                .ok()
                .headers(header)
                .build();
    }

    @DeleteMapping("/users/{userId}/appointments/{appointmentId}")
    public ResponseEntity<Object> deleteAppointmentById(@PathVariable("userId") long userId,
                                                        @PathVariable("appointmentId") long appointmentId) {
        appointmentService.deleteAppointmentById(userId, appointmentId);
        return ResponseEntity.noContent().build();
    }
}
