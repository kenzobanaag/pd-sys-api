package com.banaag.personaldev.repository;

import com.banaag.personaldev.model.Settings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettingsRepository extends JpaRepository<Settings,Long> {
}
