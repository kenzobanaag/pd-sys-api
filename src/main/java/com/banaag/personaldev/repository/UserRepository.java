package com.banaag.personaldev.repository;

import com.banaag.personaldev.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    int countByUsernameAndUserIdNot(String username, long id);
    int countByEmailAndUserIdNot(String email, long id);
}