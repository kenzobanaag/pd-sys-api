package com.banaag.personaldev.exception;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class CustomExceptionHandler {

    // 400
    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity handleBadRequestException(BadRequestException e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        return new ResponseEntity(new CustomException(e.getMessage(),badRequest, ZonedDateTime.now()), badRequest);
    }

    // 404
    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity handleResourceNotFoundException(ResourceNotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        return new ResponseEntity(new CustomException(e.getMessage(), notFound, ZonedDateTime.now()), notFound);
    }

    // 409
    @ExceptionHandler(value = {ConflictException.class})
    public ResponseEntity handleConflictException(ConflictException e) {
        HttpStatus conflict = HttpStatus.CONFLICT;
        return new ResponseEntity(new CustomException(e.getMessage(), conflict, ZonedDateTime.now()), conflict);
    }
}
