package com.banaag.personaldev.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Data
@Table(name = "settings_table")
public class Settings implements Patchable<Settings> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @Getter(AccessLevel.NONE)
    private User user;

    private boolean appAlerts;
    private boolean emailNotifications;
    @Enumerated(EnumType.ORDINAL)
    private Themes theme;

    public Settings() {
        appAlerts = false;
        emailNotifications = false;
        theme = Themes.LIGHT;
    }

    @Override
    public void patch(Settings settings) {
        emailNotifications = settings.emailNotifications;
        appAlerts = settings.appAlerts;
        if(settings.theme != null)
            theme = settings.theme;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "appAlerts=" + appAlerts +
                ", emailNotifications=" + emailNotifications +
                ", theme=" + theme +
                '}';
    }

    public enum Themes {
        LIGHT, DARK
    }
}


