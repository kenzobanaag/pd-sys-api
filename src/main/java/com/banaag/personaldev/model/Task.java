package com.banaag.personaldev.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "tasks_table")
public class Task implements Patchable<Task> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long taskId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable = false)
    @JsonIgnore
    private User user;

    private boolean done;
    private String name;
    private Date deadline;
    private int progress;
    private int priority;

    @JsonIgnore
    public boolean isComplete() {
        return !name.isBlank();
    }

    // make sure that progress is [0,100] and priority is [0,10]
    @JsonIgnore
    public boolean isValid() {
        return progress >= 0 && progress <= 100 && priority >= 0 && priority <= 10;
    }

    @Override
    public void patch(Task task) {
        if(!task.name.isBlank())
            name = task.name;
        if(task.deadline != null)
            deadline = task.deadline;
        done = task.done;
        progress = task.progress;
        priority = task.priority;
    }
}
