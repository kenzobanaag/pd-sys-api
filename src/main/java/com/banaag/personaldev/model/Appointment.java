package com.banaag.personaldev.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "appointments_table")
public class Appointment implements Patchable<Appointment> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long appointmentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable = false)
    @JsonIgnore
    private User user;

    private Date date;
    private Date startTime;
    private Date endTime;
    private String description;
    private String appointmentNotes;

    @JsonIgnore
    public boolean isComplete() {
        return date != null && startTime != null && endTime != null;
    }

    @Override
    public void patch(Appointment apt) {
        if(apt.date != null)
            date = apt.date;
        if(apt.startTime != null)
            startTime = apt.startTime;
        if(apt.endTime != null)
            endTime = apt.endTime;
        if(apt.description != null)
            description = apt.description;
        if(apt.appointmentNotes != null)
            appointmentNotes = apt.appointmentNotes;
    }

    @Override
    public String toString() {
        return " appointmentId = " + appointmentId + "\ndate = " + date + "\nstartTime: " + startTime
                + " - endTime: " + endTime;
    }
}
