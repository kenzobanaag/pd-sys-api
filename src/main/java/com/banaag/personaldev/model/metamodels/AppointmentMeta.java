package com.banaag.personaldev.model.metamodels;

import lombok.Data;

import java.sql.Date;
import java.util.Optional;

@Data
public class AppointmentMeta {
    private long userId;
    private Optional<Date> date;
    private Optional<Date> fromDate;
    private Optional<Date> toDate;
    private Optional<String> descKeywords;

    public AppointmentMeta(long userId, Optional<Date> date, Optional<Date> fromDate, Optional<Date> toDate,
                           Optional<String> descKeywords) {
        this.userId = userId;
        this.date = date;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.descKeywords = descKeywords;
    }
}