package com.banaag.personaldev.model.metamodels;

import lombok.Data;

import java.sql.Date;
import java.util.Optional;

@Data
public class JournalEntryMeta {
    private long userId;
    private Optional<Date> date;
    private Optional<Integer> moodScore;
    private Optional<Integer> fromMoodScore;
    private Optional<Integer> toMoodScore;

    public JournalEntryMeta(long userId, Optional<Date> date, Optional<Integer> moodScore,
                            Optional<Integer> fromMoodScore, Optional<Integer> toMoodScore) {
        this.userId = userId;
        this.date = date;
        this.moodScore = moodScore;
        this.fromMoodScore = fromMoodScore;
        this.toMoodScore = toMoodScore;
    }
}
