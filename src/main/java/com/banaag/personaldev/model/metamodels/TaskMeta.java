package com.banaag.personaldev.model.metamodels;

import lombok.Data;

import java.sql.Date;
import java.util.Optional;

@Data
public class TaskMeta {
    private long userId;
    private Optional<Boolean> done;
    private Optional<Integer> priority;
    private Optional<Integer> progress;
    private Optional<Date> deadline;
    private Optional<Integer> priorityGTE;
    private Optional<Integer> fromProgress;
    private Optional<Integer> toProgress;

    public TaskMeta(long userId, Optional<Boolean> done, Optional<Integer> priority,
                    Optional<Integer> progress, Optional<Date> deadline, Optional<Integer> priorityGTE,
                    Optional<Integer> fromProgress, Optional<Integer> toProgress) {
        this.userId = userId;
        this.done = done;
        this.priority = priority;
        this.progress = progress;
        this.deadline = deadline;
        this.priorityGTE = priorityGTE;
        this.fromProgress = fromProgress;
        this.toProgress = toProgress;
    }
}
