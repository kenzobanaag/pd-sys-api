package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.services.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
public class TaskControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    TaskService taskService;

    private static Task task;

    @BeforeAll
    public static void setup() {
        task = new Task();
        task.setDone(false);
        task.setName("Task 1");
    }

    @Test
    public void controllerInitTest() {
        assertNotNull(mockMvc);
    }

    @Test
    public void getAllTasks() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void getTaskById() throws Exception{
        Optional<Task> optTask = Optional.of(task);
        when(taskService.getTaskById(1L, 1L)).thenReturn(optTask);
        String apptJsonStr = this.mapper.writeValueAsString(task);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/tasks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(apptJsonStr));

        verify(taskService, times(1)).getTaskById(1L,1L);
    }

    @Test
    public void getTaskById_userNotExist() throws Exception{
        when(taskService.getTaskById(0L, 1L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/0/tasks/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(taskService, times(1)).getTaskById(0L,1L);
    }

    @Test
    public void getTaskById_idNotExist() throws Exception{
        when(taskService.getTaskById(1L, 0L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/tasks/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(taskService, times(1)).getTaskById(1L,0L);
    }

    @Test
    public void addTask() throws Exception{
        String taskJsonStr = this.mapper.writeValueAsString(task);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/users/1/tasks")
                .contentType(MediaType.APPLICATION_JSON).content(taskJsonStr)).andExpect(status().isCreated());

        assertEquals("/users/1/tasks/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void updateTask() throws Exception {
        String taskJsonStr = this.mapper.writeValueAsString(task);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.patch("/users/1/tasks/0")
                .contentType(MediaType.APPLICATION_JSON).content(taskJsonStr)).andExpect(status().isOk());

        assertEquals("/users/1/tasks/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void deleteTask() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1/tasks/1"))
                .andExpect(status().isNoContent());
        verify(taskService, times(1)).deleteTaskById(1L, 1L);
    }

    @Test
    public void deleteAllTasks() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1/tasks"))
                .andExpect(status().isNoContent());
        verify(taskService, times(1)).deleteAllTasksById(1L, Optional.empty());
    }
}
