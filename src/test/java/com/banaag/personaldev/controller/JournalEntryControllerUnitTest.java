package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.services.JournalEntryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(JournalEntryController.class)
public class JournalEntryControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    JournalEntryService journalEntryService;

    private static JournalEntry entry;

    @BeforeAll
    public static void setup() {
        entry = new JournalEntry();
        entry.setDate(new Date(System.currentTimeMillis()));
        entry.setSummary("Good Day");
    }

    @Test
    public void controllerInitTest() {
        assertNotNull(mockMvc);
    }

    @Test
    public void getAllEntries() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/journal-entries")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void getEntryById() throws Exception{
        Optional<JournalEntry> optEntry = Optional.of(entry);
        when(journalEntryService.getJournalEntryById(1L, 1L)).thenReturn(optEntry);
        String entryJsonStr = this.mapper.writeValueAsString(entry);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/journal-entries/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(entryJsonStr));

        verify(journalEntryService, times(1)).getJournalEntryById(1L,1L);
    }

    @Test
    public void getEntryById_userNotExist() throws Exception{
        when(journalEntryService.getJournalEntryById(0L, 1L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/0/journal-entries/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(journalEntryService, times(1)).getJournalEntryById(0L,1L);
    }

    @Test
    public void getEntryById_idNotExist() throws Exception{
        when(journalEntryService.getJournalEntryById(1L, 0L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/journal-entries/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(journalEntryService, times(1)).getJournalEntryById(1L,0L);
    }

    @Test
    public void addEntry() throws Exception{
        String entryJsonStr = this.mapper.writeValueAsString(entry);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/users/1/journal-entries")
                .contentType(MediaType.APPLICATION_JSON).content(entryJsonStr)).andExpect(status().isCreated());

        assertEquals("/users/1/journal-entries/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void updateEntry() throws Exception {
        String taskJsonStr = this.mapper.writeValueAsString(entry);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.patch("/users/1/journal-entries/0")
                .contentType(MediaType.APPLICATION_JSON).content(taskJsonStr)).andExpect(status().isOk());

        assertEquals("/users/1/journal-entries/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void deleteEntry() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1/journal-entries/1"))
                .andExpect(status().isNoContent());
        verify(journalEntryService, times(1)).deleteJournalEntryById(1L, 1L);
    }
}
