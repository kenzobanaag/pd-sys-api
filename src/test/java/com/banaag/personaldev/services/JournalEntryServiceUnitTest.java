package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;
import com.banaag.personaldev.repository.JournalEntryRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.JournalEntrySpecification;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
public class JournalEntryServiceUnitTest {

    @InjectMocks
    JournalEntryServiceImpl journalEntryService;

    @Mock
    JournalEntryRepository entryRepo;

    @Mock
    UserRepository userRepo;

    @Mock
    JournalEntrySpecification jEntrySpec;

    @Test
    public void serviceInitTest() {
        assertNotNull(journalEntryService);
    }

    @Test
    public void getJournalEntries_empty() {
        JournalEntryMeta meta = mock(JournalEntryMeta.class);
        when(jEntrySpec.journalQuery(meta)).thenReturn(any(Specification.class));
        when(entryRepo.findAll(jEntrySpec.journalQuery(meta))).thenReturn(new ArrayList<>());
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(0, IterableUtil.sizeOf(journalEntryService.getJournalEntries(meta)));
        verify(entryRepo, times(1)).findAll(jEntrySpec.journalQuery(meta));
    }

    @Test
    public void getJournalEntries() {
        List<JournalEntry> entries = new ArrayList<>();
        entries.add(mock(JournalEntry.class));
        entries.add(mock(JournalEntry.class));
        JournalEntryMeta meta = mock(JournalEntryMeta.class);
        when(jEntrySpec.journalQuery(meta)).thenReturn(any(Specification.class));
        when(entryRepo.findAll(jEntrySpec.journalQuery(meta))).thenReturn(entries);
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(entries.size(), IterableUtil.sizeOf(journalEntryService.getJournalEntries(meta)));
        verify(entryRepo, times(1)).findAll(jEntrySpec.journalQuery(meta));
    }

    @Test
    public void getJournalEntries_userNotExist() {
        JournalEntryMeta meta = mock(JournalEntryMeta.class);
        when(userRepo.existsById(meta.getUserId())).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> journalEntryService.getJournalEntries(meta));

        verify(entryRepo, times(0)).findAll(jEntrySpec.journalQuery(meta));
    }

    @Test
    public void getJournalEntryById() {
        long userId = 1L;
        long entryId = 1L;
        when(entryRepo.findByUserUserIdAndEntryId(userId, entryId))
                .thenReturn(Optional.of(mock(JournalEntry.class)));

        Optional<JournalEntry> userFromService = journalEntryService.getJournalEntryById(userId, entryId);
        assertTrue(userFromService.isPresent());
        verify(entryRepo, times(1)).findByUserUserIdAndEntryId(userId, entryId);
    }

    @Test
    public void getJournalEntryById_userNotExist() {
        long userId = 1L;
        long entryId = 1L;
        when(entryRepo.findByUserUserIdAndEntryId(userId, entryId))
                .thenReturn(Optional.empty());

        Optional<JournalEntry> userFromService = journalEntryService.getJournalEntryById(userId, entryId);
        assertTrue(userFromService.isEmpty());
        verify(entryRepo, times(1)).findByUserUserIdAndEntryId(userId, entryId);
    }

    @Test
    public void getJournalEntryById_idNotExist() {
        long userId = 1L;
        long entryId = 1L;
        when(entryRepo.findByUserUserIdAndEntryId(userId, entryId))
                .thenReturn(Optional.empty());

        Optional<JournalEntry> userFromService = journalEntryService.getJournalEntryById(userId, entryId);
        assertTrue(userFromService.isEmpty());
        verify(entryRepo, times(1)).findByUserUserIdAndEntryId(userId, entryId);
    }

    @Test
    public void addJournalEntry() {
        long userId = 1L;
        long entryId = 1L;

        JournalEntry entry = mock(JournalEntry.class);
        when(entry.getEntryId()).thenReturn(entryId);
        User user = mock(User.class);
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(entry.isComplete()).thenReturn(true);
        when(entryRepo.save(entry)).thenReturn(entry);

        assertEquals(entryId, journalEntryService.addJournalEntry(userId, entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(1)).save(entry);
    }

    @Test
    public void addJournalEntry_userNotExist() {
        long userId = 1L;

        JournalEntry entry = mock(JournalEntry.class);
        User user = mock(User.class);
        when(userRepo.findById(userId)).thenReturn(Optional.empty());
        when(entry.isComplete()).thenReturn(true);

        assertThrows(ResourceNotFoundException.class,
                () -> journalEntryService.addJournalEntry(userId, entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(0)).save(entry);
    }

    @Test
    public void addJournalEntry_incomplete() {
        long userId = 1L;

        JournalEntry entry = mock(JournalEntry.class);
        User user = mock(User.class);
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(entry.isComplete()).thenReturn(false);
        when(entryRepo.save(entry)).thenReturn(entry);

        assertThrows(BadRequestException.class,
                () -> journalEntryService.addJournalEntry(userId, entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(0)).save(entry);
    }

    @Test
    public void updateJournalEntry() {
        long userId = 1L;
        long entryId = 1L;
        JournalEntry entry = mock(JournalEntry.class);
        User user = mock(User.class);
        when(entry.getEntryId()).thenReturn(entryId);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(entryRepo.findById(userId)).thenReturn(Optional.of(entry));
        when(entryRepo.save(entry)).thenReturn(entry);

        assertEquals(entryId, journalEntryService.updateJournalEntry(userId, entryId,entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(1)).findById(entryId);
        verify(entryRepo, times(1)).save(entry);
    }

    @Test
    public void updateJournalEntry_userNotExist() {
        long userId = 1L;
        long entryId = 1L;
        JournalEntry entry = mock(JournalEntry.class);

        when(userRepo.findById(userId)).thenReturn(Optional.empty());
        when(entryRepo.findById(userId)).thenReturn(Optional.of(entry));
        when(entryRepo.save(entry)).thenReturn(entry);

        assertThrows(ResourceNotFoundException.class,
                () -> journalEntryService.updateJournalEntry(userId, entryId,entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(0)).findById(entryId);
        verify(entryRepo, times(0)).save(entry);
    }

    @Test
    public void updateJournalEntry_idNotExist() {
        long userId = 1L;
        long entryId = 1L;
        JournalEntry entry = mock(JournalEntry.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(entryRepo.findById(userId)).thenReturn(Optional.empty());
        when(entryRepo.save(entry)).thenReturn(entry);

        assertThrows(ResourceNotFoundException.class,
                () -> journalEntryService.updateJournalEntry(userId, entryId,entry));
        verify(userRepo, times(1)).findById(userId);
        verify(entryRepo, times(1)).findById(entryId);
        verify(entryRepo, times(0)).save(entry);
    }

    @Test
    public void deleteJournalEntryById() {
        long userId = 1L;
        long entryId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);
        when(entryRepo.existsById(userId)).thenReturn(true);

        assertDoesNotThrow(() -> journalEntryService.deleteJournalEntryById(userId, entryId));

        verify(userRepo, times(1)).existsById(userId);
        verify(entryRepo, times(1)).existsById(entryId);
        verify(entryRepo, times(1)).deleteById(userId);
    }

    @Test
    public void deleteJournalEntryById_userNotExist() {
        long userId = 1L;
        long entryId = 1L;

        when(userRepo.existsById(userId)).thenReturn(false);
        when(entryRepo.existsById(userId)).thenReturn(true);

        assertThrows(ResourceNotFoundException.class,
                () -> journalEntryService.deleteJournalEntryById(userId, entryId));

        verify(userRepo, times(1)).existsById(userId);
        verify(entryRepo, times(0)).existsById(entryId);
        verify(entryRepo, times(0)).deleteById(entryId);
    }

    @Test
    public void deleteJournalEntryById_idNotExist() {
        long userId = 1L;
        long entryId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);
        when(entryRepo.existsById(userId)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class,
                () -> journalEntryService.deleteJournalEntryById(userId, entryId));

        verify(userRepo, times(1)).existsById(userId);
        verify(entryRepo, times(1)).existsById(entryId);
        verify(entryRepo, times(0)).deleteById(userId);
    }
}
